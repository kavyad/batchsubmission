import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

import { BatchSubmissionService } from '../services/batch-submission.service';

@Component({
  selector: 'app-batch-listing',
  templateUrl: './batch-listing.component.html',
  styleUrls: ['./batch-listing.component.css']
})
export class BatchListingComponent implements OnInit {
  public batchSubmissions : any

  constructor(private batchService: BatchSubmissionService, private router:Router, private spinner: NgxSpinnerService) {
      this.spinner.show();
  }

  ngOnInit() {
    this.batchService.getbatchSubmissions().then(data => {
      //console.log(typeof(data.body));
      if(data != {}){
        this.batchSubmissions = JSON.parse(Object(data)["body"]);
        this.spinner.hide();
      }
    })
  }

  getBatchedSubmissions(){
    this.batchService.getbatchSubmissions().then(data => {
      console.log(data);
      this.batchSubmissions = JSON.parse(Object(data)["body"]);
    })
  }

  redirect(batchId){
    this.router.navigate(['/batched-submission-list'],{queryParams:{id:window.btoa(batchId)}});
  }

}

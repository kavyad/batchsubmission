import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchedSubmissionListComponent } from './batched-submission-list.component';

describe('BatchedSubmissionListComponent', () => {
  let component: BatchedSubmissionListComponent;
  let fixture: ComponentFixture<BatchedSubmissionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchedSubmissionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchedSubmissionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

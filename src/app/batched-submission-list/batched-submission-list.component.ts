import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BatchSubmissionService } from '../services/batch-submission.service';

import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-batched-submission-list',
  templateUrl: './batched-submission-list.component.html',
  styleUrls: ['./batched-submission-list.component.css']
})
export class BatchedSubmissionListComponent implements OnInit {
  batchId;
  batchedSubmissionData;
  batch={
    offerCode: '',
    batchFile: 20,
    batchCount: '',
    status:'',
    batchType:'',
    cardProgram:'',
    createdDate:'',
    batchId:''
  };
  offerCode;
  fileName;
  fileURL;
  constructor(private batchService: BatchSubmissionService, private route: ActivatedRoute, private router: Router, private spinner: NgxSpinnerService) {
    this.spinner.show();
    this.route.queryParams.subscribe(params => {
      this.batchId = window.atob(params['id']);
      this.batchService.getbatchedSubmissions(this.batchId).then(data => {
        console.log(data);
        if(data != ''){
          this.batchedSubmissionData = data;//JSON.parse(Object(data)["body"]);
          this.offerCode = this.batchedSubmissionData[0].offerId;
          this.fileName = this.batchedSubmissionData[0].batch_file;
          this.downloadFile();
          this.spinner.hide();
        }
        else{
          this.offerCode = '';
        }
      });
    });
  }

  ngOnInit() {}

  downloadFile(){
    this.spinner.show();
    this.batchService.downloadFile(this.batchId, this.fileName).then(data => {
      console.log(typeof(data));
      if(data != ''){
        // let result = [];
        // result = JSON.parse(data);
        this.fileURL = Object(data)["downloadUrl"];//data.downloadUrl;
        this.spinner.hide();
        // let a = document.createElement('a');
        //   a.setAttribute('href', JSON.parse(data));//.href = encodeURI(data);
        //   a.setAttribute('download', this.fileName);
        //   document.body.appendChild(a);
        //   a.click();
        //   document.body.removeChild(a);
      }
    });
  }

  copyToSFTP(){
    //this.spinner.show();
    this.batchService.copyToSFTP(this.fileName).then(data => {
      this.spinner.hide();
      console.log((data));
    });
    // var uploadCSVFileOnSftpServer = function(next, s3FileStreamContent) {
    // var filePath = this.fileURL;
    // var Client = require('ssh2').Client;
    // var connSettings = {
    //     host: 's-a34b0bb2dbc04a878.server.transfer.us-east-2.amazonaws.com',
    //     port: 22,
    //     username: 'sftpTestUser',
    //     password: 'ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAqbESUGhAFgwmJ6O7bJpLmjeNySYLJiiL98gSwVuty3Ix78bcRmxeyMcH++qdzIIttvYaCbXf/Voll8FZrRWdav4Bd4eZLdvQWwaEFeHywvb/opcNWy/tJ4p/GtoXGlSvipY66YcDLp9aSNJ0EgSmhDX1aDMtRZ5DAS0K6IJAz8PDOPQYfus6EFwTA/mTVqtfG3C0EZid1BjcKeovjWTu1YnEy9yGpdu2LvJ4wxg91faTYB/A3FcZy8wnrMGZSJEoVaJk09A9GvYsCpzx+1pFMwajKePq6Q9fXxmZ2j7gTRqZzOJfFbvkhiOy+tcBmUzleQLOSqlNYNLO3AH0L2+2VQ== rsa-key-20200110'
    // };
    // var conn = new Client();
    // conn.on('ready', function() {
    //     conn.sftp(function(err, sftp) {
    //         if (err) {
    //             console.log("Errror in connection", err);
    //         } else {
    //             console.log("Connection established", sftp);
    //             var options = Object.assign({}, {
    //                 encoding: 'utf-8'
    //             }, true);
    //             var stream = sftp.createWriteStream(filePath, options);
    //             var data = stream.end(s3FileStreamContent);
    //             stream.on('close', function() {
    //                 console.log("- file transferred succesfully");
    //                 conn.end();
    //                 next(null, true);
    //             });
    //         }
    //     });
    // }).connect(connSettings);}
  }

}

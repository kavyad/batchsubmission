import { Component, OnInit } from '@angular/core';

import { UserRolesService } from '../services/user-roles.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-user-roles',
  templateUrl: './user-roles.component.html',
  styleUrls: ['./user-roles.component.css']
})
export class UserRolesComponent implements OnInit {
  roles;
  rolesValues = {};
  arrayValur = [];
  constructor(private userRolesService:UserRolesService, private spinner: NgxSpinnerService) {
    this.spinner.show();
   }

  ngOnInit() {
    this.userRolesService.getUserRoles().then(data => {
      console.log(data);
      this.roles = data;
      this.spinner.hide();
    });
  }

  selectRoles(roleId, isChecked, value) {
    console.log(this.rolesValues);
    if (isChecked) {
      if(this.arrayValur.indexOf(value) < 0){
        this.arrayValur.push(value);
      }
    } else {
      let index = this.arrayValur.indexOf(value);
      this.arrayValur.splice(index, 1);
    }
    if(this.arrayValur.length > 0){
      this.rolesValues[roleId] = this.arrayValur;
    }
  }

  saveRole(){
    this.spinner.show();
    this.userRolesService.saveRole(this.rolesValues).then(data => {
      this.spinner.hide();
      console.log(data);
    });
  }

}

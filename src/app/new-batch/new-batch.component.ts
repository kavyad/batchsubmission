import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { BatchSubmissionService } from '../services/batch-submission.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-new-batch',
  templateUrl: './new-batch.component.html',
  styleUrls: ['./new-batch.component.css']
})
export class NewBatchComponent implements OnInit {

  batch={
    offerCode: '',
    promotionStatus:'',
    client:'',
    cardIssuer: '',
    cardProgram: '',
    amount: 20,
    totalAmount: '',
    onbefore: ''
  };
  message;
  check = false;
  stopBatching = false;
  offerIds;
  clientIds;
  batchs = [];

  constructor(private batchService: BatchSubmissionService,
    private router: Router, private spinner: NgxSpinnerService,private http: HttpClient) { }

  ngOnInit() {
    this.message = '';
     this.getOfferIdDetails();
     this.getClientDetail();
    //this.user = new User();
  }

  // getOfferIdDetails(){
  //     this.http.get("https://8p1q4zs910.execute-api.us-east-2.amazonaws.com/staging/offervalidate").subscribe(res => {
  //       this.offerIds=res;
  //       console.log(this.offerIds);
  //     });
  //   }
  //   getClientDetail(){
  //       this.http.get(" https://3eq1bie8qf.execute-api.us-east-2.amazonaws.com/default/getClientDetails").subscribe(res => {
  //         this.clientIds=res;
  //         console.log(this.clientIds);
  //       });
  //     }

   getOfferIdDetails(){
     this.batchService.getOfferIdDetails().then(data => {
       console.log(data);
        this.offerIds=data;

     })
   }


   getClientDetail(){
     this.batchService.getClientDetail().then(data => {
       this.clientIds=data;
       console.log(data);
        })
   }

    getAllocatedAmount(total, amount){
      return (parseInt(total) - parseInt(amount));
    }

  saveBatchSubmissions() {
    console.log(this.batch.totalAmount == '' && this.batch.offerCode.length == 0);
    if(this.batch.totalAmount == '' || this.batch.offerCode.length == 0 || this.batch.cardIssuer.length == 0 || this.batch.cardProgram.length == 0 ||
    this.batch.onbefore.length == 0 || this.batch.promotionStatus.length == 0 || this.batch.client.length == 0  ){
      alert("Enter Details");
    }
    else{
        this.spinner.show();
        let formBody = [];
        if(this.batch.offerCode.length > 0){
          for(let i=0; i<this.batch.offerCode.length ; i++){
            let formData = {
              "client":this.batch.client,
              "offerCode":this.batch.offerCode[i],
              "amount":"",
              "totalAmount":this.batch.totalAmount,
             "cardIssuer":this.batch.cardIssuer,
             "cardProgram":  this.batch.cardProgram,
             "onbefore":this.batch.onbefore,
             "promotionStatus":this.batch.promotionStatus
            }
            formBody.push(formData);
          }
        }
        else{
          formBody.push(this.batch);
        }
        let batchingDetails = {"formBody":formBody, "check":"initial"}
        this.batchService.saveBatchSubmissions(batchingDetails).then(data => {
        this.check = true;

        if(JSON.parse(Object(data)["body"].includes('No Data'))) {
          this.stopBatching = true;
          this.check = false;
        }
        Object(data)["body"].forEach(element => {
          this.batchs.push(JSON.parse(element));
        });
        //this.batchs = Object(data)["body"];
        this.spinner.hide();
        console.log(data);// this.router.navigate(['user-listing']);
      });
    }
  }

  finalBatchSubmissions() {
    this.spinner.show();
    this.message = '';
    console.log(this.batch);
    let formBody = [];
    if(this.batch.offerCode.length > 0){
      for(let i=0; i<this.batch.offerCode.length ; i++){
        let formData = {
          "offerCode":this.batch.offerCode[i],
          "amount":"",
          "totalAmount":this.batch.totalAmount,
           "client":this.batch.client,
           "cardIssuer":this.batch.cardIssuer,
           "cardProgram":  this.batch.cardProgram,
           "onbefore":this.batch.onbefore,
           "promotionStatus":this.batch.promotionStatus
        }
        formBody.push(formData);
      }
    }
    else{
      formBody.push(this.batch);
    }
    console.log(formBody);
    let batchingDetails = {"formBody":formBody}
    this.batchService.saveBatchSubmissions(batchingDetails).then(data => {

      this.message = "Sucessfully Batched";
      this.spinner.hide();
      this.router.navigate(['batch-listing']);
      //console.log(data);// this.router.navigate(['user-listing']);
    });
  }

  Cancel(){
    this.batch.offerCode = '';
    this.batch.totalAmount = '';
    this.batch.amount = 20;
    this.check = false;
    this.message = '';
    this.stopBatching = false;
    this.batchs = [];
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";
import { DataTableModule } from 'angular-6-datatable';

import { AppComponent } from './app.component';
import { BatchListingComponent } from './batch-listing/batch-listing.component';
import { NewBatchComponent } from './new-batch/new-batch.component';
import { BatchSubmissionService } from './services/batch-submission.service';
import { BatchedSubmissionListComponent } from './batched-submission-list/batched-submission-list.component';
import { UserRolesComponent } from './user-roles/user-roles.component';
import { UserRolesService } from './services/user-roles.service';

const appRoutes: Routes = [
  { path: '', redirectTo: '/batch-listing', pathMatch: 'full' },
  { path: 'batch-listing', component: BatchListingComponent },
  { path: 'new-batch', component: NewBatchComponent },
  { path: 'batched-submission-list', component: BatchedSubmissionListComponent },
  { path: 'roles', component: UserRolesComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    BatchListingComponent,
    NewBatchComponent,
    BatchedSubmissionListComponent,
    UserRolesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true}
    ),
    NgxSpinnerModule,
    DataTableModule
  ],
  providers: [
    BatchSubmissionService,
    UserRolesService
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }

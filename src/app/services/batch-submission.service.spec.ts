import { TestBed } from '@angular/core/testing';

import { BatchSubmissionService } from './batch-submission.service';

describe('BatchSubmissionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BatchSubmissionService = TestBed.get(BatchSubmissionService);
    expect(service).toBeTruthy();
  });
});

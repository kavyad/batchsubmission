import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable()
export class BatchSubmissionService {

  constructor(private http: HttpClient) { }

  getbatchSubmissions(){
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq', 'x-api-key': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq'});
      let options = { headers: headers };
      this.http.post('https://fx6vfswjq3.execute-api.us-east-2.amazonaws.com/batching/upload-to-s3', {"formBody":{"type":"get"}}, options).subscribe((response) => {
          console.log(response);
          resolve(response);
        }, err => {
          console.log(JSON.stringify(err));
      });
    });
  }

  saveBatchSubmissions(batchSubmissions) {
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq', 'x-api-key': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq'});
      let options = { headers: headers };
      this.http.post('https://fx6vfswjq3.execute-api.us-east-2.amazonaws.com/batching/upload-to-s3', batchSubmissions, options).subscribe((response) => {
          console.log(response);
          resolve(response);
        }, err => {
          console.log(JSON.stringify(err));
      });
    });
  }

  getbatchedSubmissions(batchId){
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq', 'x-api-key': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq'});
      let options = { headers: headers };
      this.http.post('https://sc6ew9tahj.execute-api.us-east-2.amazonaws.com/staging/batchedsubmissions', {id:batchId, download:false}, options).subscribe((response) => {
          console.log(response);
          resolve(response);
        }, err => {
          console.log(JSON.stringify(err));
      });
    });
  }

  downloadFile(batchId, fileName){
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq', 'x-api-key': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq'});
      let options = { headers: headers };
      this.http.post('https://sc6ew9tahj.execute-api.us-east-2.amazonaws.com/staging/batchedsubmissions', {id:batchId, download:true, fileName: fileName}, options).subscribe((response) => {
          console.log(response);
          resolve(response);
        }, err => {
          console.log(JSON.stringify(err));
      });
    });
  }

  copyToSFTP(fileName){
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq', 'x-api-key': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq'});
      let options = { headers: headers };
      this.http.post('https://lsa3ntg8ef.execute-api.us-east-2.amazonaws.com/default/sftptransferfiles', {fileName}, options).subscribe((response) => {
          console.log(response);
          resolve(response);
        }, err => {
          console.log(JSON.stringify(err));
      });
    });
  }

    getOfferIdDetails(){
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq', 'x-api-key': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq'});
      let options = { headers: headers };
      this.http.get('https://8p1q4zs910.execute-api.us-east-2.amazonaws.com/staging/offervalidate', options).subscribe((response) => {
          console.log(response);
          resolve(response);
        }, err => {
          console.log(JSON.stringify(err));
      });
    });
  }


  getClientDetail(){
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq', 'x-api-key': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq'});
      let options = { headers: headers };
      this.http.get('https://1lvg4twqgb.execute-api.us-east-2.amazonaws.com/staging/getclientdetails',  options).subscribe((response) => {
          console.log(response);
          resolve(response);
        }, err => {
          console.log(JSON.stringify(err));
      });
    });
  }

}

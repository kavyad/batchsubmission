import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserRolesService {

  constructor(private http: HttpClient) { }

  getUserRoles(){
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq', 'x-api-key': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq'});
      let options = { headers: headers };
      this.http.get('https://x8upz03pua.execute-api.us-east-2.amazonaws.com/staging/authenticate', options).subscribe((response) => {
          console.log(response);
          resolve(response);
        }, err => {
          console.log(JSON.stringify(err));
      });
    });
  }

  saveRole(rolesValues){
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq', 'x-api-key': 'LwCPjpqye59hgONbe6IrD6VK5HDpGrVJ4fhE2Qmq'});
      let options = { headers: headers };
      this.http.post('https://x8upz03pua.execute-api.us-east-2.amazonaws.com/staging/authenticate', rolesValues, options).subscribe((response) => {
          console.log(response);
          resolve(response);
        }, err => {
          console.log(JSON.stringify(err));
      });
    });
  }

}
